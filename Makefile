all:
	pdflatex *.tex
	pdflatex *.tex

.PHONY: clean
clean:
	rm *.aux *.bbl *.blg *.log *.out *.toc *.pdf
